import {ADD_DISH_ADMIN, DELETE_DISH_ADMIN, EDIT_DISH_ADMIN, INIT_DISH} from "./actionTypes";

export const addDish = (title, price) => ({type: ADD_DISH_ADMIN, title, price});

export const editDish = (title, price, img) => ({type: EDIT_DISH_ADMIN, title, price, img});

export const deleteDish = (title, price) => ({type: DELETE_DISH_ADMIN, title, price});

export const initDishes = () => ({type: INIT_DISH});