import {
  ADD_DISH_ADMIN,
  DELETE_DISH_ADMIN,
  EDIT_DISH_ADMIN,
  INIT_DISH, ORDER_FAILURE,
  ORDER_REQUEST,
  ORDER_SUCCESS, TOGGLE_MODAL
} from "../actions/actionTypes";

const initialState = {
  orderDishes: {},
  totalPrice: 0,
  delivery: 150,
  loader: false,
  error: null,
};

// const newDishes = Object.keys(this.state.dishes).map((dish) => {
//   return this.setState({orderDishes: dish})
// });



const parseDishes = (dishes) => {
   const orderDishes = {};
  Object.keys(dishes).forEach(id => {
    orderDishes[dishes[id].title] = {count: 0, price:dishes[id].price, id}
  });
  return orderDishes;
};

const reducerOrders = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DISH_ADMIN:
      return {
        ...state,
        orderDishes: {
          ...state.orderDishes,
          [action.title]: {...state.orderDishes[action.title], count : state.orderDishes[action.title].count + 1}
        },
        totalPrice: (state.totalPrice) + parseInt(action.price)

        // ingredients: {
        //   ...state.ingredients,
        //   [action.dishName]: state.ingredients[action.dishName] + 1
        // },
      };
    case  EDIT_DISH_ADMIN:
    case DELETE_DISH_ADMIN:
      return {
        ...state,
        orderDishes: {
          ...state.orderDishes,
          [action.title]: {...state.orderDishes[action.title], count : state.orderDishes[action.title].count - 1}
        },
        totalPrice: (state.totalPrice) - parseInt(action.price)
      };
    case INIT_DISH:
    case ORDER_REQUEST:
      return {...state, loading: true};
    case ORDER_SUCCESS:
      return {...state, loading: false, orderDishes: parseDishes(action.dishes)};
    case ORDER_FAILURE:
      return {...state, loading: false, error: action.error};
    case TOGGLE_MODAL:
      return {...state, modalVisible: !state.modalVisible};
    default:
      return state;
  }

};

export default reducerOrders;