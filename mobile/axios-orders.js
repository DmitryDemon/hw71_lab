import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://hw71labchekruz.firebaseio.com/'
});

export default instance;