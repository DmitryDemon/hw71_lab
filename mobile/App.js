import React from 'react';
import {
  StyleSheet, Text, TextInput, View, Button,
  TouchableOpacity, FlatList, Image, Modal, ScrollView
} from 'react-native';


import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import {connect, Provider} from 'react-redux';
import reducerDishes from "./store/reducers/reducerDishes";
import {createList, toggleModal} from "./store/actions/actionOrder";
import reducerOrders from "./store/reducers/reducerOrders";
import {addDish, deleteDish} from "./store/actions/actionDishes";



class App extends React.Component {

  state = {
    placeName: '',
    dishes: [],
  };

  componentDidMount() {
    this.props.createList();
  }

  placeNameChangedHandler = text => {
    this.setState({placeName: text});
  };

  placeAddedHandler = () => {

    this.setState({
      places: [
        ...this.state.places,
        {
          key: String(Math.random()),
          place: this.state.placeName
        }
      ]
    });
  };



  render() {
    console.log(Object.keys(this.props.dishes).map(item=>{
      return this.props.dishes[item].price
    }));
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={{fontSize: 25,}}>Turtle Pizza</Text>
        </View>
        <View style={styles.flatContainer}>
        <FlatList
          data={Object.keys(this.props.dishes).map((dish) => (this.props.dishes[dish]))}
          renderItem={({item}) => {
            return <TouchableOpacity
              onPress={() => this.props.addDish(item.title, item.price)}
            >
              <View style={styles.Cart}>
                <Image
                  source={{uri: item.img}}
                  style={{width: 40, height: 40, marginRight: 10}}
                />
                <Text style={{fontSize: 20,}}>{item.title}</Text>
                <Text style={{fontWeight: 'bold',}}> {item.price} KGS</Text>
              </View>
            </TouchableOpacity>
          }}
        />
        </View>
        <View style={styles.priceContainer}>
          <Text style={{margin: 5, fontSize: 20,}}>Order total:{this.props.totalPrice} KGS</Text>
          <Button title="Checkout" onPress={this.props.toggleModal}/>
        </View>

        <Modal
            animationType="fade"
            transparent={false}
            visible={this.props.modalVisible}
            onRequestClose={this.props.toggleModal}>
          <View style={{margin: 5, backgroundColor: '#fff'}}>
            <Text>Your order:</Text>
            <View slyle={{flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between'}}>
              {Object.keys(this.props.orderDishes).map((order, key)=>{
                const countDish = this.props.orderDishes[order].count;
                const price = this.props.orderDishes[order].price;
                return countDish > 0 ?
                <View  key={key} >

                  <Text>
                    {order} X {countDish}  = {price * countDish}
                  </Text>
                  <Button title={'X'} onPress={()=>this.props.deleteDish(order, price)}/>
                </View> : null
              })}

            </View>
            <View>
              <Text>Delivery {this.props.delivery} KGS</Text>
            </View>
            <View>
              <Text>Total: {this.props.totalPrice}KGS</Text>
            </View>
            <Button title={'Cancel'} onPress={this.props.toggleModal}/>
            <Button title={'Order'} onPress={this.props.toggleModal}/>
          </View>
        </Modal>

      </View>
    );
  }
}



const mapStateToProps = state => ({
  modalVisible: state.dish.modalVisible,
  dishes: state.dish.dishes,
  orderDishes: state.ord.orderDishes,
  totalPrice: state.ord.totalPrice,
  loading: state.ord.loading,
  delivery: state.ord.delivery,
  // orderDishes: state.ord.orderDishes,
});

const mapDispatchToProps = dispatch => ({
  toggleModal: () => dispatch(toggleModal()),
  createList: () => dispatch(createList()),
  addDish: (title, price) => dispatch(addDish(title, price)),
  deleteDish: (title, price) => dispatch(deleteDish(title, price)),
});

const AppConnected = connect(mapStateToProps, mapDispatchToProps)(App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    paddingHorizontal: 10,
    paddingTop: 30
  },
  flatContainer: {
    flex: 8,
  },
  Cart: {
    padding: 10,
    marginTop: 10,
    height: 60,
    backgroundColor: '#E6E6FA',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'row',
    borderBottomColor: '#4B0082',
    borderBottomWidth: 3,
  },
  priceContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    paddingBottom: 20,
    borderTopColor: '#4B0082',
    borderTopWidth: 3,
  }
});

const rootReducer = combineReducers({
  ord: reducerOrders,
  dish: reducerDishes,
});

const store = createStore(
  rootReducer,
  applyMiddleware(thunkMiddleware)
);

const index = () => (
  <Provider store={store}>
    <AppConnected />
  </Provider>
);


export default index;