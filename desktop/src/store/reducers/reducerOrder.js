import {
    ORDER_FAILURE,
    ORDER_REQUEST,
    ORDER_SUCCESS} from "../actions/actionTypes";

const initialState = {
    dishes: {},
    loading: false,
    error: null,
    ordered: false,
};

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_REQUEST:
            return {...state, loading: true};
        case ORDER_SUCCESS:
            return {...state, loading: false, dishes: action.dishes};
        case ORDER_FAILURE:
            return {...state, loading: false, error: action.error};
        default:
            return state;
    }
};

export default orderReducer;