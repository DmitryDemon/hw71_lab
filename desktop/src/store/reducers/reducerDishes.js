import {ADD_DISH_ADMIN, DELETE_DISH_ADMIN, EDIT_DISH_ADMIN, INIT_DISH} from "../actions/actionTypes";

const initialState = {
  dishes: {},
  loader: false,
  error: null,
};

const reducerDishes = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DISH_ADMIN:
      return {
        ...state,

      };
    case  EDIT_DISH_ADMIN:
    case DELETE_DISH_ADMIN:
    case INIT_DISH:
    default:
      return state;
  }
};

export default reducerDishes;