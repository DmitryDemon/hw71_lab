import React, { Component } from 'react';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import MenuBuilder from "./containers/Dishes/Dishes";
import Orders from "./containers/Orders/Orders";

import './App.css';
import AddedDishes from "./components/AddedDishes/AddedDishes";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={MenuBuilder} />
          <Route path="/orders" component={Orders} />
          <Route path="/added-dishes" component={AddedDishes} />
          <Route render={() => <h1>No found!</h1>} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
