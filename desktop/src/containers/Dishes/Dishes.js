import React, {Component} from 'react';
import Button from "../../components/UI/Button/Button";

import './Dishes.css';
import {connect} from "react-redux";
import Spinner from "../../components/UI/Spinner/Spinner";
import ListDishes from "../../components/ListDishes/ListDishes";
import {createList, createOrder, deleteOrder} from "../../store/actions/actionOrder";

class Dishes extends Component {
    componentDidMount(){
        this.props.createList()
    }

  purchaseContinue = () => {
    // this.props.initOrder();
    this.props.history.push('/added-dishes');
  };

  render() {

    return (
      <div className="Dishes">
        <h1>Dishes</h1>
        <Button btnType="Primary" onClick={this.purchaseContinue}>Add new Dish</Button>
        <div className="Dishes-content">
          {this.props.loading ? <Spinner />  : <ListDishes
            dishes={this.props.dishes} remove={(dish) => this.props.deleteOrder(dish)}
          />}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state =>({
    dishes: state.ord.dishes,
    loading: state.ord.loading,
});
const mapDispatchToProps = dispatch =>({
    createList: () => dispatch(createList()),
    createOrder: (orderData, history) => dispatch(createOrder(orderData,history)),
    deleteOrder: (orderData) => dispatch(deleteOrder(orderData))
});

export default connect(mapStateToProps,mapDispatchToProps)(Dishes);