import React, {Component} from 'react';
import Button from "../UI/Button/Button";

import './AddedDishes.css';
import {connect} from "react-redux";
import {createOrder} from "../../store/actions/actionOrder";

class AddedDishes extends Component {

    state = {
        name: 'Added dishes',
        title: '',
        price: '',
        img: '',
    };

  orderHandler = event => {
        event.preventDefault();

        const orderData = {
            title: this.state.title,
            price: this.state.price,
            img: this.state.img
        };

      this.props.createOrder(orderData, this.props.history);
    };

  valueChanged = event => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

  render() {
    return (
      <div className="AddedDishes">
        <h3>{this.state.name}</h3>
        <form onSubmit={this.orderHandler}>
          <input className="Input" type="text" name="title" placeholder="Title dishes"
                 value={this.state.title} onChange={this.valueChanged}/>
          <input className="Input" type="text" name="price" placeholder="Price dishes"
                 value={this.state.price} onChange={this.valueChanged}/>
          <input className="Input" type="text" name="img" placeholder="Image dishes"
                 value={this.state.img} onChange={this.valueChanged}/>
          <Button type='submit' btnType="Success">Save</Button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
    products: state.ord.products,
    loading: state.ord.loading,
});

const mapDispatchToProps = dispatch => ({
    createOrder: (orderData, history) => dispatch(createOrder(orderData,history)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddedDishes);

