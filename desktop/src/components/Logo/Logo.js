import React from 'react';

import './Logo.css';


const Logo = () => (
  <div className="Logo">
    <h1>Turtle Pizza Admin</h1>
  </div>
);


export default Logo;