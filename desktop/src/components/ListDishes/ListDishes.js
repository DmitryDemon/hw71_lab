import React from 'react';
import Button from "../UI/Button/Button";

import './ListDishes.css';

const ListDishes = props => {
  if (props.dishes === null){
    return <h1>Add dishes!</h1>
  } else  {
    return (
      Object.keys(props.dishes).map((dish,key) =>{

        const oneDish = props.dishes[dish];
        console.log(oneDish.title);
        return <div key={key} className='ListDishes'>
          <img src={oneDish.img} alt="dish"/>
          <p className="ListDishes-title">{oneDish.title}</p>
          <p className="ListDishes-price">{oneDish.price} KGS</p>
          <Button btnType='Warning'>Edit</Button>
          <Button onClick={() => props.remove(dish)} btnType='Danger'>Delete</Button>
        </div>
      })

    );
  }

};


export default ListDishes;

