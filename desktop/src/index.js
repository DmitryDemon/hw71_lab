import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {Provider} from "react-redux";

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import BrowserRouter from "react-router-dom/es/BrowserRouter";
import reducerDishes from "./store/reducers/reducerDishes";
import reducerOrder from "./store/reducers/reducerOrder";

const rootReducer = combineReducers({
  dish: reducerDishes,
  ord: reducerOrder,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunkMiddleware))
);

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
